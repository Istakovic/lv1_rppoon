using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notes notes;
            notes = new Notes();
            Console.WriteLine(notes.Text);
            Console.WriteLine(notes.Author);
            Console.WriteLine("");

            notes = new Notes("Dont go outside!", "Dzon Travolta", 5);
            Console.WriteLine(notes.Text);
            Console.WriteLine(notes.Author);
            Console.WriteLine("");

            notes = new Notes("Dzon Travolta", 5);
            Console.WriteLine(notes.Text); 
            Console.WriteLine(notes.Author);
            Console.WriteLine(notes.ToString());

            TimeNote timeNote = new TimeNote();
            Console.WriteLine(timeNote.ToString());
            
        }
    }
}
