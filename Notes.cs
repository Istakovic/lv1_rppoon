using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_1
{
    class Notes
    {
        private string text;
        private string author;
        private int priority;
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }
        public Notes()
        {
            this.text = "Stay safe!";
            this.author = "Dzon Travolta";
            this.priority = 5;
        }
        public Notes(string text,string author,int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }
        public Notes(string author, int priority)
        {
            this.text = "STAY SAFE!";
            this.author = author;
            this.priority = priority;
        }
        public string GetText()
        {
            return this.text;
        }             
        public string GetAuthor()
        {
            return this.author;
        }
        public int GetPriority()
        {
            return this.priority;
        }
        public void SetText(string text) { this.text = text; }
        private void SetAuthor(string author) { this.author = author; }
        public void SetPriority(int priority) { this.priority = priority; }

        public override string ToString() 
        { return this.Text+ " " + this.Author +" "+ this.Priority; }
        
    }
}
