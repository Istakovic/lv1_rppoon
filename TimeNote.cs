using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_1
{
    class TimeNote:Notes
    {
        private DateTime time;
           
        public TimeNote()
        {
            this.time = DateTime.Now;
        }
        public TimeNote(string text, string author, int priority, DateTime time) : base(text, author, priority)
        {
            this.time = time;
        }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return base.ToString()+" "+this.time;
        }
    }
}
